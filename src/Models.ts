export interface Device {
  id: string;
  lastUpdate: string;
  cpu: number;
  memory: number;
  cores: number;
  total_memory: number;
  arch: string;
  hostname: string;
  version: string;
  type: string;
  platform: string;
  connected: boolean;
  cpuusage: number[];
  memoryusage: number[];
}

export interface Update {
  device_id: string;
  cpu: number;
  memory: number;
}

export interface WsMessage {
  devices?: Device[];
  updates?: Update[];
}
